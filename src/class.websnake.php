<?php
/*
###########################################################################################################
##                                                                                                       ##
##                        WEBSNAKE - PYTHON MEDIA TOOL FOR WEB DEVELOPMENT                               ##
##                                                                                                       ##
##          Write PHP, do PYTHON! Have you ever tried to use PHP to process media issues, as image       ##
##          rendering, for example? It is painful and the result is almost always less than expected.    ##
##          Websnake Media Tool, was created to fulfill this deficit. It's developed in PYTHON, which is ##
##          much more indicated to process media, but the usage is entirely through an easy              ##
##          PHP interface, which requires just a basic knowledge in PHP and Object Oriented Programming. ##
##          Enjoy using and make powerful web applications with basic web development skills.            ##
##                                                                                                       ##
##                                                                                                       ##
##          Websnake Python Media Tool - Copyright (c) 2016 Gabriel Valentoni Guelfi                     ##
##                                                                                                       ##
##          >>> CONTACT DEVELOPER:                                                                       ##
##          >>> Gabriel Guelfi                                                                           ##
##          >>> Website: http://gabrielguelfi.com.br                                                     ##
##          >>> Email: gabriel.valguelfi@gmail.com                                                       ##
##          >>> Skype: gabriel-guelfi                                                                    ##
##                                                                                                       ##
##                                                                                                       ##
##          This file is part of Websnake Python Media Tool.                                             ##
##                                                                                                       ##
##          Websnake Python Media Tool is free software: you can redistribute it and/or modify           ##
##          it under the terms of the GNU General Public License as published by                         ##
##          the Free Software Foundation, either version 3 of the License.                               ##
##                                                                                                       ##
##          Websnake Python Media Tool is distributed in the hope that it will be useful,                ##
##          but WITHOUT ANY WARRANTY; without even the implied warranty of                               ##
##          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                ##
##          GNU General Public License for more details.                                                 ##
##                                                                                                       ##
##          You should have received a copy of the GNU General Public License                            ##
##          along with this copy of Websnake Python Media Tool under the name of LICENSE.pdf.            ##
##          If not, see <http://www.gnu.org/licenses/>.                                                  ##
##                                                                                                       ##
##          Using, modifying and/or running this software or any of its contents, implies consent        ##
##          to the terms and conditions explicit within the license, mentioned above.                    ##
##                                                                                                       ##
###########################################################################################################


* >>> WEBSNAKE PHP INTERFACE - MANUAL <<<
* This class receive data from your PHP application, send it to Websnake python engine and return an object with
*the output data. Visit "https://bitbucket.org/gabriel-guelfi/websnake" for further information.

*/
class Websnake {

    /*
    # >>>> PRIVATE CONSTRUCT METHOD:
    #
    # Defines main directories and paths:
    */
    private static function init() {
        define('WS_ROOT', dirname(__FILE__));
        define('WS_INCLUDES', WS_ROOT.'/includes/');
        define('WS_TEMP', WS_ROOT.'/tmp/');
    }
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    /*
    # >>>> PUBLIC STATIC METHOD execute:
    #
    # Receives request data. Call "setArgs()" method to turn it into an API friendly json argument list.
    #If data is too large, write it on a json file and sets file's path on the argument list.
    #
    # Prepare shell command to call the python framework, setting the argument list, and execute the command.
    #
    # Receives the python engine response's ouput and returns it.
    #
    # If "$debug" is set true, it adds passed arguments(raw and encoded) and the generated command in output object
    #before it is returned. If "$stop_propagation" is also set true, it will not return the output, intead,
    #it will be printed on screen, with all debug details and the script's execution will exit.
    */
    public static function execute($request_data, $debug = false, $stop_propagation = false) {
        self::init();
        
        $args = self::setArgs($request_data->args);

        $cmd = "python " . WS_ROOT . "/framework.py " . $request_data->module . " " . $request_data->method . " '" . $args . "'";
        if (strlen($cmd) >= 4096) {
            $args = str_replace("\n", "", $args);
            file_put_contents(WS_ROOT . '/tmp/wsinput.json', $args);
            $args = '{"__largeinput__":"' . WS_ROOT . '/tmp/wsinput.json' . '","debug":'.($debug ? "1" : "0").'}';
            $cmd = "python " . WS_ROOT . "/framework.py " . $request_data->module . " " . $request_data->method . " '" . $args . "'";
        }
        
        $ws_output;
        exec($cmd, $ws_output);
        $output = new stdClass();
        for ($i = 0; $i < count($ws_output); $i = $i + 2) {
            $output->$ws_output[$i] = $ws_output[$i + 1];
        }

        if($debug){
            $r = array(
                'args' => $request_data->args,
                'encoded_args' => $args,
                'cmd' => $cmd,
                'output' => $output
            );

            if($stop_propagation){
                echo "<pre>";
                print_r($r);
                echo "</pre>";
            } else return $r;
        }else return $output;
    }
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    /*
    # >>>> PRIVATE STATIC METHOD setArgs:
    #
    # This method is recursive. It formats data objects in a API friendly json type argument list and
    #returns it.
    */
    private static function setArgs($args,$put_keys = true,$embrace = true){
        if(isset($args['bin'])){
            if($args['bin'] === true){
                $args['bin'] = 1;
            }
            elseif($args['bin'] === false){
                $args['bin'] = 0;
            }
        }

        $r = '';
        if($embrace)
            $r = '{';
        foreach($args as $key => $a){
            if($put_keys)
                $r .= '"'.$key.'":';
            if(is_numeric($a))
                $r .= $a;
            elseif(is_string($a))
                $r .= '"'.$a.'"';
            elseif(is_array($a)){
                if((bool)count(array_filter(array_keys($a), 'is_string'))){
                    $r .= '{'.self::setArgs($a,true,false).'}';
                }else{
                    $r .= '['.self::setArgs($a,false,false).']';
                }
            }
            elseif(is_object($a))
                $r .= '{'.self::setArgs($a,true,false).'}';
            $r .= ',';
        }
        $r = rtrim($r,',');
        if($embrace)
            $r .= '}';
        $r = str_replace("\n","\\n",$r);
        $r = str_replace("\r","\\r",$r);
        return $r;
    }

}
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
?>