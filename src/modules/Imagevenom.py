###########################################################################################################
##                                                                                                       ##
##                        WEBSNAKE - PYTHON MEDIA TOOL FOR WEB DEVELOPMENT                               ##
##                                                                                                       ##
##          Write PHP, do PYTHON! Have you ever tried to use PHP to process media issues, as image       ##
##          rendering, for example? It is painful and the result is almost always less than expected.    ##
##          Websnake Media Tool, was created to fulfill this deficit. It's developed in PYTHON, which is ##
##          much more indicated to process media, but the usage is entirely through an easy              ##
##          PHP interface, which requires just a basic knowledge in PHP and Object Oriented Programming. ##
##          Enjoy using and make powerful web applications with basic web development skills.            ##
##                                                                                                       ##
##                                                                                                       ##
##          Websnake Python Media Tool - Copyright (c) 2016 Gabriel Valentoni Guelfi                     ##
##                                                                                                       ##
##          >>> CONTACT DEVELOPER:                                                                       ##
##          >>> Gabriel Guelfi                                                                           ##
##          >>> Website: http://gabrielguelfi.com.br                                                     ##
##          >>> Email: gabriel.valguelfi@gmail.com                                                       ##
##          >>> Skype: gabriel-guelfi                                                                    ##
##                                                                                                       ##
##                                                                                                       ##
##          This file is part of Websnake Python Media Tool.                                             ##
##                                                                                                       ##
##          Websnake Python Media Tool is free software: you can redistribute it and/or modify           ##
##          it under the terms of the GNU General Public License as published by                         ##
##          the Free Software Foundation, either version 3 of the License.                               ##
##                                                                                                       ##
##          Websnake Python Media Tool is distributed in the hope that it will be useful,                ##
##          but WITHOUT ANY WARRANTY; without even the implied warranty of                               ##
##          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                ##
##          GNU General Public License for more details.                                                 ##
##                                                                                                       ##
##          You should have received a copy of the GNU General Public License                            ##
##          along with this copy of Websnake Python Media Tool under the name of LICENSE.pdf.            ##
##          If not, see <http://www.gnu.org/licenses/>.                                                  ##
##                                                                                                       ##
##          Using, modifying and/or running this software or any of its contents, implies consent        ##
##          to the terms and conditions explicit within the license, mentioned above.                    ##
##                                                                                                       ##
###########################################################################################################


from core.Websnake import Websnake
from includes.PIL import Image
from includes.PIL import ImageFont
from includes.PIL import ImageDraw
import struct

class Imagevenom(Websnake):
    def __init__(self, _params):
        self.set_params(_params)
        self.tmpPath = self.root+"/tmp/"
        
    def img_writefile(self, _filename = False, _data = False):
        bool = False
        if not _filename or not _data:
            opId = self.uniqid()
            bool = True
            self.require_args(['savepath','data'],['unicode','unicode'])
            _filename = self.params['savepath']+"/"+opId
            _data = self.params['data']
        
        imgtype = _data[_data.find("/")+1:_data.find(";")]
        
        outputpath = _filename+'.'+imgtype
        outputfile = open(outputpath,'w')
        outputfile.write(_data.replace("data:image/"+imgtype+";base64,","").decode('base64'))
        outputfile.close()
        resPath = _filename+'.png'
        convert = Image.open(outputpath)
        self.os.remove(outputpath)
        convert.save(resPath, "PNG")
        
        if bool:
            print "operation_id"
            print opId
            print "file_path"
            print resPath
        else:
            return resPath

    def checkBin(self, _opId, _img):
        if not self.params.has_key('bin'):
            self.params['bin'] = False

        if bool(self.params['bin']):
            resPath = self.img_writefile(self.tmpPath+_opId, _img)
        else:
            resPath = _img

        return resPath
        
    def crop_img(self):
        self.require_args(['bg','x','y','w','h'],['unicode','int','int','int','int'])
        
        resId = str(self.uniqid())
        bgPath = self.checkBin(resId,self.params['bg'])
        
        img = Image.open(bgPath)
        x = int(self.params['x'])
        y = int(self.params['y'])
        w = int(self.params['x'])+int(self.params['w'])
        h = int(self.params['y'])+int(self.params['h'])
        box = (x, y, w, h)
        img = img.crop(box)
        resPath = self.tmpPath+resId+".png"
        img.save(resPath, "PNG")
        
        self.jobdone(img, resId)
        
    def glue_imgs(self):
        self.require_args(['bg','fgs'],['unicode','list'])
            
        resId = str(self.uniqid())
        bgPath = self.checkBin(resId,self.params['bg'])
            
        bg = Image.open(bgPath)
        # MELHORIA: para cada fg, parametro bin opcional para fg's em codebyte
        for fgData in self.params['fgs']:
            fg = Image.open(fgData['fg'])
            bg.paste(fg, (int(fgData['x']),int(fgData['y'])), fg.convert('RGBA'))
            
        resPath = self.tmpPath+resId+".png"
        bg.save(resPath, "PNG")
        
        self.jobdone(bg, resId)
        
    def txt2img(self):
        self.require_args(['text','textcolor','fontfile','fontsize'],['unicode','unicode','unicode','int'])
        
        font = ImageFont.truetype(self.params['fontfile'], self.params['fontsize'], encoding = "unic")
        img = Image.new("RGBA", (100,100), (255, 255, 255, 0))
        draw = ImageDraw.Draw(img)
        textlines = self.params['text'].splitlines()
        w,h = draw.textsize(max(textlines,key=len),font)
        w = w + (w*0.03)
        h = len(textlines) * self.params['fontsize']
        h = h + (h*0.2)
        img = img.resize((w, h))
        draw = ImageDraw.Draw(img)

        y = 0
        for line in textlines:
            draw.text((0,y), line, self.params['textcolor'], font)
            y = y + self.params['fontsize']
        
        resId = str(self.uniqid())
        resPath = self.tmpPath+resId+".png"
        img.save(resPath, "PNG")
        
        self.jobdone(img, resId)

    def resize_img(self):
        self.require_args(['img','width','height'],['unicode','int', 'int'])

        opId = str(self.uniqid())
        imgPath = self.checkBin(opId,self.params['img'])

        newsize = (self.params['width'],self.params['height'])

        img = Image.open(imgPath)
        img = img.resize(newsize,Image.ANTIALIAS)
        resPath = self.tmpPath+opId+".png"
        img.save(resPath, "PNG")

        self.jobdone(img, opId)

    def img_thumb(self):
        self.require_args(['img','thumb_size'],['unicode','int'])

        opId = str(self.uniqid())
        imgPath = self.checkBin(opId, self.params['img'])

        img = Image.open(imgPath)
        (imgW,imgH)  = img.size

        imgW = float(imgW)
        imgH = float(imgH)
        self.params['thumb_size'] = float(self.params['thumb_size'])

        if imgW > imgH:
            if imgW >= self.params['thumb_size']:
                rel = self.params['thumb_size'] / imgW
                newSize = (int(self.params['thumb_size']),int(rel * imgH))
            else:
                return
        elif imgH > imgW:
            if imgH >= self.params['thumb_size']:
                rel = self.params['thumb_size'] / imgH
                newSize = (int(rel * imgW),int(self.params['thumb_size']))
            else:   
                return
        else:
            if imgW >= self.params['thumb_size']:
                newSize = (int(self.params['thumb_size']),int(self.params['thumb_size']))
            else:
                return

        img = img.resize(newSize,Image.ANTIALIAS)
        resPath = self.tmpPath+opId+".png"
        img.save(resPath, "PNG")

        self.jobdone(img, opId)

    def img_mask(self):
        self.require_args(['img','mask_bg','mask_matrix'],['unicode','dict','dict'])

        opId = str(self.uniqid())
        imgPath = self.checkBin(opId,self.params['img'])

        img = Image.open(imgPath)
        img = img.convert('RGBA')
        img_px = img.load()

        if self.params['mask_bg']['type'] == 'color':
            mask = Image.new('RGBA', img.size, self.params['mask_bg']['value'])
        else:
            mask = Image.new('RGBA', img.size, '#000000')

            if not self.params['mask_bg'].has_key('bin'):
                self.params['mask_bg']['bin'] = False
            if bool(self.params['mask_bg']['bin']):
                maskBgPath = self.img_writefile(self.tmpPath+"/"+self.uniqid(), self.params['mask_bg']['value'])
                maksBgTmpfile = maskBgPath
            else:
                maskBgPath = self.params['mask_bg']['value']

            maskBg = Image.open(maskBgPath)
            maskBg = maskBg.convert('RGBA')

            if maskBg.size[0] >= mask.size[0]:
                limitx = 1
            else:
                limitx = int(self.math.ceil(float(mask.size[0])/float(maskBg.size[0])))
            if maskBg.size[1] >= mask.size[1]:
                limity = 1
            else:
                limity = int(self.math.ceil(float(mask.size[1])/float(maskBg.size[1])))

            for k in range(0,limitx):
                for l in range(0,limity):
                    mask.paste(maskBg,((k * maskBg.size[0]),(l * maskBg.size[1])),maskBg.convert('RGBA'))


        mask_px = mask.load()

        channel_matrix = {
        'red':0,
        'green':1,
        'blue':2,
        'alpha':3
        }

        mask_offset = {
        'top':img.size[1],
        'right':0,
        'bottom':0,
        'left':img.size[0]
        }

        for i in range(img.size[0]):
            for j in range(img.size[1]):
                if self.params['mask_matrix'].has_key('channels') and self.params['mask_matrix'].has_key('colors'):
                    for channel in self.params['mask_matrix']['channels']:
                        for color in self.params['mask_matrix']['colors']:
                            rgb_str = color.replace("#","")+"ff"
                            color_data = struct.unpack("BBBB",rgb_str.decode('hex'))
                            if not img_px[i,j][channel_matrix[channel['name']]] == int(channel['value']) and not img_px[i,j] == color_data:
                                mask_px[i,j] = (0,0,0,0)
                                if i < mask_offset['left']:
                                    mask_offset['left'] = i;
                                if i > mask_offset['right']:
                                    mask_offset['right'] = i;
                                if j < mask_offset['top']:
                                    mask_offset['top'] = j;
                                if j > mask_offset['bottom']:
                                    mask_offset['bottom'] = j;
                elif self.params['mask_matrix'].has_key('channels') and not self.params['mask_matrix'].has_key('colors'):
                    for channel in self.params['mask_matrix']['channels']:
                        if not img_px[i,j][channel_matrix[channel['name']]] == int(channel['value']):
                            mask_px[i,j] = (0,0,0,0)
                            if i < mask_offset['left']:
                                mask_offset['left'] = i;
                            if i > mask_offset['right']:
                                mask_offset['right'] = i;
                            if j < mask_offset['top']:
                                mask_offset['top'] = j;
                            if j > mask_offset['bottom']:
                                mask_offset['bottom'] = j;
                elif not self.params['mask_matrix'].has_key('channels') and self.params['mask_matrix'].has_key('colors'):
                    for color in self.params['mask_matrix']['colors']:
                        rgb_str = color.replace("#","")+"ff"
                        color_data = struct.unpack("BBBB",rgb_str.decode('hex'))
                        if not img_px[i,j] == color_data:
                            mask_px[i,j] = (0,0,0,0)
                            if i < mask_offset['left']:
                                mask_offset['left'] = i;
                            if i > mask_offset['right']:
                                mask_offset['right'] = i;
                            if j < mask_offset['top']:
                                mask_offset['top'] = j;
                            if j > mask_offset['bottom']:
                                mask_offset['bottom'] = j;
                else:
                    print('Websnake@ModuleError')
                    print('Imagevenom@ArgumentError: No option passed in the matrix. No information from which create the mask')
                    for (dirpath, subdirnames, filenames) in self.os.walk(self.tmpPath):
                        for fname in filenames:
                            self.os.remove(self.tmpPath+"/"+fname)
                        break
                    self.sys.exit()

        resPath = self.tmpPath+opId+".png"
        mask.save(resPath,"PNG")

        print('mask_offset')
        print(self.encode_json(mask_offset))

        self.jobdone(mask, opId)
        
    def jobdone(self, _imgobj, _resid):
        resPath = self.tmpPath+_resid+".png"
        
        print('operation_id')
        print(_resid)
        
        if self.params.has_key("savepath"):
            savepath = self.params['savepath']+"/"+_resid+".png"
            _imgobj.save(savepath, "PNG")
            print('file_path')
            print(savepath)
        else:
            resImg = open(resPath, 'rb')
            print('image_data')
            print("data:image/png;base64,"+resImg.read().encode('base64').replace('\n', ''))
            resImg.close()

        self.os.remove(resPath)