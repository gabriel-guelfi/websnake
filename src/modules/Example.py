###########################################################################################################
##                                                                                                       ##
##                        WEBSNAKE - PYTHON MEDIA TOOL FOR WEB DEVELOPMENT                               ##
##                                                                                                       ##
##          Write PHP, do PYTHON! Have you ever tried to use PHP to process media issues, as image       ##
##          rendering, for example? It is painful and the result is almost always less than expected.    ##
##          Websnake Media Tool, was created to fulfill this deficit. It's developed in PYTHON, which is ##
##          much more indicated to process media, but the usage is entirely through an easy              ##
##          PHP interface, which requires just a basic knowledge in PHP and Object Oriented Programming. ##
##          Enjoy using and make powerful web applications with basic web development skills.            ##
##                                                                                                       ##
##                                                                                                       ##
##          Websnake Python Media Tool - Copyright (c) 2016 Gabriel Valentoni Guelfi                     ##
##                                                                                                       ##
##          >>> CONTACT DEVELOPER:                                                                       ##
##          >>> Gabriel Guelfi                                                                           ##
##          >>> Website: http://gabrielguelfi.com.br                                                     ##
##          >>> Email: gabriel.valguelfi@gmail.com                                                       ##
##          >>> Skype: gabriel-guelfi                                                                    ##
##                                                                                                       ##
##                                                                                                       ##
##          This file is part of Websnake Python Media Tool.                                             ##
##                                                                                                       ##
##          Websnake Python Media Tool is free software: you can redistribute it and/or modify           ##
##          it under the terms of the GNU General Public License as published by                         ##
##          the Free Software Foundation, either version 3 of the License.                               ##
##                                                                                                       ##
##          Websnake Python Media Tool is distributed in the hope that it will be useful,                ##
##          but WITHOUT ANY WARRANTY; without even the implied warranty of                               ##
##          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                ##
##          GNU General Public License for more details.                                                 ##
##                                                                                                       ##
##          You should have received a copy of the GNU General Public License                            ##
##          along with this copy of Websnake Python Media Tool under the name of LICENSE.pdf.            ##
##          If not, see <http://www.gnu.org/licenses/>.                                                  ##
##                                                                                                       ##
##          Using, modifying and/or running this software or any of its contents, implies consent        ##
##          to the terms and conditions explicit within the license, mentioned above.                    ##
##                                                                                                       ##
###########################################################################################################

# Import Websnake core class(required):
from core.Websnake import Websnake

# Class declaration of the module(Must extends Websnake class):
class Example(Websnake):
    # Constructor method("self.set_params()" statement required):
    def __init__(self, _params):
        self.set_params(_params)
        
    # Simple "Hello World" example:
    def helloworld(self):
        print "Websnake@Hello_World: Websnake Example's module says hello!"
        
    # Print a value passed in argument named "arg"(Throw a FATAL if no argument named "arg" is found):
    def print_arg(self):
        self.require_args()
            
        print "Websnake@HelloWorld: Value passed down as argument = "+str(self.params['arg'])+" of datatype "+self.php_datatypes[type(self.params['arg']).__name__]
        
    # Print php datatype names in Websnake Framework:
    def listdatatypes(self):
        types = self.php_datatypes
        print"\nHow is it named php datatypes in Websnake Framework:"
        print "PHP   ->   WEBSNAKE"
        print "///////////////////"
        for t in types:
            print types[t]+"   ->   "+t
            print "--------------------------------"
        print "\n"
        