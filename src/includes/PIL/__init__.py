#
# The Python Imaging Library.
# $Id$
#
# package placeholder
#
# Copyright (c) 1999 by Secret Labs AB.
#
# See the README file for information on usage and redistribution.
#

# ;-)
import os

modules_path = os.path.abspath(os.path.dirname( __file__ ))

for dirname, dirnames, filenames in os.walk(modules_path):
    filenames = [f.replace(".py", "") for f in filenames]
    _all__ = filenames