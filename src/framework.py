###########################################################################################################
##                                                                                                       ##
##                        WEBSNAKE - PYTHON MEDIA TOOL FOR WEB DEVELOPMENT                               ##
##                                                                                                       ##
##          Write PHP, do PYTHON! Have you ever tried to use PHP to process media issues, as image       ##
##          rendering, for example? It is painful and the result is almost always less than expected.    ##
##          Websnake Media Tool, was created to fulfill this deficit. It's developed in PYTHON, which is ##
##          much more indicated to process media, but the usage is entirely through an easy              ##
##          PHP interface, which requires just a basic knowledge in PHP and Object Oriented Programming. ##
##          Enjoy using and make powerful web applications with basic web development skills.            ##
##                                                                                                       ##
##                                                                                                       ##
##          Websnake Python Media Tool - Copyright (c) 2016 Gabriel Valentoni Guelfi                     ##
##                                                                                                       ##
##          >>> CONTACT DEVELOPER:                                                                       ##
##          >>> Gabriel Guelfi                                                                           ##
##          >>> Website: http://gabrielguelfi.com.br                                                     ##
##          >>> Email: gabriel.valguelfi@gmail.com                                                       ##
##          >>> Skype: gabriel-guelfi                                                                    ##
##                                                                                                       ##
##                                                                                                       ##
##          This file is part of Websnake Python Media Tool.                                             ##
##                                                                                                       ##
##          Websnake Python Media Tool is free software: you can redistribute it and/or modify           ##
##          it under the terms of the GNU General Public License as published by                         ##
##          the Free Software Foundation, either version 3 of the License.                               ##
##                                                                                                       ##
##          Websnake Python Media Tool is distributed in the hope that it will be useful,                ##
##          but WITHOUT ANY WARRANTY; without even the implied warranty of                               ##
##          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                ##
##          GNU General Public License for more details.                                                 ##
##                                                                                                       ##
##          You should have received a copy of the GNU General Public License                            ##
##          along with this copy of Websnake Python Media Tool under the name of LICENSE.pdf.            ##
##          If not, see <http://www.gnu.org/licenses/>.                                                  ##
##                                                                                                       ##
##          Using, modifying and/or running this software or any of its contents, implies consent        ##
##          to the terms and conditions explicit within the license, mentioned above.                    ##
##                                                                                                       ##
###########################################################################################################


# Import main class Websnake:
from core.Websnake import Websnake

# The framework initializes the proccess make the first data arrangements and call the method from the module asked:
class Framework(Websnake):
    
    # Data arrangements:
    def __init__(self):
        self.module = str(self.sys.argv[1])
        self.action = str(self.sys.argv[2])
        
        try:
            self.params = str(self.sys.argv[3])
        except IndexError:
            self.params = "0"
            
        self.exec_action(self.module, self.action, self.params)
   
    # Import the module and call the method asked:
    def exec_action(self, _module, _action, _params):
        exec("from modules."+_module.capitalize()+" import *")
        exec("obj = "+_module.capitalize()+"("+_params+")")
        result = eval("obj."+_action+"()")
        return result
    
# Initialize:
framework = Framework()