# WEBSNAKE - PYTHON MEDIA TOOL FOR WEB DEVELOPMENT - Version 1.0 Beta #

Write PHP, do PYTHON! Have you ever tried to use PHP to process media issues, as image
rendering, for example? It is painful and the result is almost always less than expected.
Websnake Media Tool, was created to fulfill this deficit. It's developed in PYTHON, which is
much more indicated to process binary data, but the usage is entirely through an easy
PHP interface, which requires just a basic knowledge in PHP and Object Oriented Programming.
Enjoy using and make powerful web applications with basic web development skills.

Websnake Python Media Tool is an Open Source, Free software. And is under the terms and conditions of
GNU/GPL license. You not only have permission to distribute it, but is also encouraged to. But read the license, before. A copy of the GNU/GPL license is attached with the source code, within the root folder.


# Knowledge required: #

 1. Object Oriented Programming concept(OOP)
 2. Basic PHP language programming


# Quick Summary #

 1. Importing into your PHP project;
 2. Basic Usage;
 3. Modules description and its methods;


# MANUAL: #

### 1. Importing Websnake to your PHP project: ###
   
1.1) Simply put the Websnake main folder inside any directory within your project. There's no further configuration needed.


### 2. Basic Usage: ###

2.1) Inside your php application, include the php layer class in any script, making Websnake available within that script:


```
#!css+php

include "path/to/websnake/src/class.websnake.php";
```


2.2) Create a standard object and sets the properties and its values as follows:


```
#!css+php

$stdobj = new stdClass();
$stdobj->module = "name_of_module"; /* The name of the module you want to use */
$stdobj->method = "modules_method"; /* The method you want to call from the module */

$stdobj->args = array(              /* The data that you want to pass to the method. */
    'data1' => "value1",            /* Note that the main array must be an associative one. */
    'data2' => 2,
    'data3' => array("value4", 5)
        );
```


2.3) Call the php static method "execute" passing the object created before:


```
#!css+php

$response = Websnake::execute($stdobj);
```


2.4) Use the value returned in response:


```
#!css+php

print_r($response); /* It's return is a standard object */
```


### 3. Modules and its methods: ###

**3.1) Module "Imagevenom":**
It's a graphic management module and its purposes is for image manipulation and rendering. The methods within it is listed below.


*3.1.1 Method img_writefile():*
```
#!php


Description:
 * Create a image file from bytecode data. The name of the file will be an unique id.

---------------------------------------------------------------

Parameters:

 * savepath:String > Absolute path to the directory where the image file will be created;
 * data:String > The bytecode data to be written;

---------------------------------------------------------------

Returns:
 * operation_id:String > The unique id which identifies the operation. It is also the name of the file 
created;
 * file_path:String > The absolute path to the created file;




```

*3.1.2) Method crop_img():*
```
#!php


Description:
 * Crop an image at position specified with the given dimension.

---------------------------------------------------------------

Parameters:
 * bg:String > The absolute path to the image file or the bytecode of the image to be cropped;
 * x:Integer > X start position;
 * y:Integer > Y start position;
 * w:Integer > Width of the cropped result;
 * h:Integer > Height of the cropped result;
 * bin:Boolean(optional) > TRUE for bytecode input. FALSE for file path input;
 * savepath:String(optional) > The absolute path to output file directory;

---------------------------------------------------------------

Returns:
 * operation_id:String > The unique id which identifies the operation. It is also the name of the file 
created, if there is one;
 * file_path:String > If savepath argument is specified, the absolute path to the file created;
 * image_data:String > If savepath is not specified, the bytecode data of the cropped image;
```


*3.1.3) Method glue_imgs():*
```
#!php


Description:
 * Override image(s) to a given background.

---------------------------------------------------------------

Parameters:
 * bg:String > The absolute path to the background image file, or the bytecode of it.
 * fg:Array > An array containing associatives arrays, as follows:
array(
'fg' => 'path'(String), 
'x' => x position(Integer), 
'y' => y position(Integer)
)
 * bin:Boolean(optional) > TRUE for bytecode input. FALSE for file path input;
 * savepath:String(optional) > The absolute path to output file directory;

---------------------------------------------------------------

Returns:
 * operation_id:String > The unique id which identifies the operation. It is also the name of the file 
created, if there is one;
 * file_path:String > If savepath argument is specified, the absolute path to the file created;
 * image_data:String > If savepath is not specified, the bytecode data of the image's composition;
```

*3.1.4) Method txt2img():*
```
#!php


Description:
 * Creates an image from a text with the specified font and provided styles

---------------------------------------------------------------

Parameters:
 * text:String > The message, the text itself;
 * textcolor:String > Hexadecimal value for a RGB color;
 * fontfile:String > Absolute path to the font-family file, which will be used to render the image;
 * fontsize:Integer > The value in pixels of the font size.
 * savepath:String(optional) > The absolute path to output file directory;

---------------------------------------------------------------

Returns:
 * operation_id:String > The unique id which identifies the operation. It is also the name of the file 
created, if there is one;
 * file_path:String > If savepath argument is specified, the absolute path to the file created;
 * image_data:String > If savepath is not specified, the bytecode data of the text image;
```


*3.1.5) Method resize_img():*
```
#!php


Description:
 * Resize an image with a specified width and height

---------------------------------------------------------------

Parameters:
 * img:String > The absolute path to the image file or the bytecode of the image to be resized;
 * width:Integer > Width expected of the result;
 * height:Integer > Height expected of the result;
 * bin:Boolean(optional) > TRUE for bytecode input. FALSE for file path input;
 * savepath:String(optional) > The absolute path to output file directory;

---------------------------------------------------------------

Returns:
 * operation_id:String > The unique id which identifies the operation. It is also the name of the file 
created, if there is one;
 * file_path:String > If savepath argument is specified, the absolute path to the file created;
 * image_data:String > If savepath is not specified, the bytecode data of the resized image;
```


*3.1.6) Method img_thumb():*
```
#!php


Description:
 * Creates a thumbnail from a given image with the maximum thumb size specified.

---------------------------------------------------------------

Parameters:
 * img:String > The absolute path to the image file or the bytecode of the original image;
 * thumb_size:Integer > The maximum width or height expected of the result;
 * bin:Boolean(optional) > TRUE for bytecode input. FALSE for file path input;
 * savepath:String(optional) > The absolute path to output file directory;

---------------------------------------------------------------

Returns:
 * operation_id:String > The unique id which identifies the operation. It is also the name of the file 
created, if there is one;
 * file_path:String > If savepath argument is specified, the absolute path to the file created;
 * image_data:String > If savepath is not specified, the bytecode data of the thumbnail created;
```


*3.1.7) Method img_mask():*
```
#!php


Description:
 * Creates a mask for a given image from the image's colors and color channels with the specified options.

---------------------------------------------------------------

Parameters:
 * img:String > The absolute path to the image file or the bytecode of the image;
 * mask_bg:Array > It is the options of the background of the mask that will be created. It's an associative array containing data, as follows:
array(
'type' => 'type'(String)  /*Accept 2 values: 'color' or 'image'.*/
'value' => 'value'(String), /*If color type, the hexadecimal of the color. If image type, the absolute path to the image file or it's bytecode*/
'bin' => TRUE or FALSE(Boolean) /*TRUE for bytecode input. FALSE for file path input */
);
 * mask_matrix:Array > This option sets the colors and/or channels, within the image, from which the mask will be created for this image. It's an associative array containing numeric and/or associative arrays, as follows:
array(
'channels' => array(   /*OPTIONAL Create the mask from the specified channels with the specified values*/
    array(
        'name' => 'name'(String), /*The name of the color channel. Ex.: 'alpha' */
        'value' => valueOfChannel(Integer) /* Value of the channel from which mask will be created. From 0 to 255 */
        )
    ),
'colors' => array( /*OPTIONAL Create the mask from the specified hexadecimal colors values*/
    '#hexOfColor'(String) /* Ex.: #ff0000 (full red) */
    )
);
 * bin:Boolean(optional) > TRUE for bytecode input. FALSE for file path input;
 * savepath:String(optional) > The absolute path to output file directory;

---------------------------------------------------------------

Returns:
 * operation_id:String > The unique id which identifies the operation. It is also the name of the file 
created, if there is one;
 * file_path:String > If savepath argument is specified, the absolute path to the image's mask file created;
 * image_data:String > If savepath is not specified, the bytecode data of the image's mask created;
```


### Who am i? ###

My name is Gabriel Valentoni Guelfi. I'm an I.T. professional, specialized in PHP and web development. And a technology enthusiastic.

#### Contact me: ####
* Skype: gabriel-guelfi
* Email: gabriel.valguelfi@gmail.com
* Website: [gabrielguelfi.com.br](http://gabrielguelfi.com.br)
* Blog: [Develog](http://blog.gabrielguelfi.com.br)
* Linkedin: [Gabriel Guelfi](https://br.linkedin.com/in/gabriel-valentoni-guelfi-30ba8b4b)